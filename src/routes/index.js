import React from "react";
import {Route, Switch} from "react-router-dom";

import LightSystem from "./clubSystem/index";

const App = ({match}) => (
  <div className="gx-main-content-wrapper">
    <Switch>
      <Route path={`${match.url}club-system`} component={LightSystem}/>      
    </Switch>
  </div>
);

export default App;
