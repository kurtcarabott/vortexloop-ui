import React from "react";
import {Breadcrumb, Card} from "antd";

const LightSystem = () => {
  return (
    <Card className="gx-card" title="Club System - Light System">
      <Breadcrumb>
        <Breadcrumb.Item><span className="gx-link"><a href="/">Home</a></span></Breadcrumb.Item>
        <Breadcrumb.Item><span className="gx-link"><a href="/club-system">Club System</a></span></Breadcrumb.Item>
        <Breadcrumb.Item><span className="gx-link"><a href="/club-system/light-system">Light System</a></span></Breadcrumb.Item>                
      </Breadcrumb>
    </Card>
  );
};

export default LightSystem;