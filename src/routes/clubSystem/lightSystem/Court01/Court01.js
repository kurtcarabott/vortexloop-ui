import React, {useState} from "react";
import {Breadcrumb, TimePicker, Alert, Card, Col, Row, Switch, Calendar} from "antd";
import moment from "moment";

const Court01 = () => {

  const [value, setValue] = useState(moment('2020-10-01'));
  const [selectedValue, setSelectedValue] = useState(moment('2020-10-01'));

  const onSelect = (value) => {
    setValue(value);
    setSelectedValue(value);
    console.log(value.format('YYYY-MM-DD'));
  };
  const onPanelChange = (value) => {
    setValue(value);
    console.log(value.format('YYYY-MM-DD'));
  };

  const timeFormat = 'HH:mm';

  return (
    <Row id="crt01Base">
      <Col span={24}>
          <Row>
          <Col xl={12} lg={12} md={12} sm={12} xs={24}>
            <Card className="gx-card" title="Light System - Court 01">
              <Breadcrumb id="breadcrumbCrt01">
                <Breadcrumb.Item><span className="gx-link"><a href="/">Home</a></span></Breadcrumb.Item>
                <Breadcrumb.Item><span className="gx-link"><a href="/club-system">Club System</a></span></Breadcrumb.Item>
                <Breadcrumb.Item><span className="gx-link"><a href="/club-system/light-system">Light System</a></span></Breadcrumb.Item>
                <Breadcrumb.Item><span className="gx-link"><a href="/club-system/light-system/court01">Court 01</a></span></Breadcrumb.Item>        
              </Breadcrumb>
            </Card>
          </Col>
          <Col xl={12} lg={12} md={12} sm={12} xs={24}>
            <center>
            <Card className="gx-card gx-mb-3" title="Manual or Auto">              
              <Switch checkedChildren="Auto" unCheckedChildren="Manual" defaultChecked />
            </Card>
            </center>
          </Col>          
        </Row>  
        <Alert message={`You selected date: ${selectedValue && selectedValue.format('YYYY-MM-DD')}`} />
        <Row>          
          <Col xl={6} lg={12} md={16} sm={20} xs={24}>
            <Card className="gx-card gx-com-calendar-card" title="Choose Date">              
              <Calendar className="gx-com-calendar" fullscreen={false} value={value} onSelect={onSelect} onPanelChange={onPanelChange} />
            </Card>
          </Col>
          <col>
          
          </col>                   
        </Row> 
        <Row>          
          <Col xl={4} lg={12} md={16} sm={20} xs={24}>
            <Card className="gx-card" title="Choose Time From">              
              <TimePicker size="large" format={timeFormat} minuteStep={15} secondStep={60}/>
            </Card>
          </Col>
          <Col xl={4} lg={12} md={16} sm={20} xs={24}>
            <Card className="gx-card" title="Choose Time To">              
              <TimePicker size="large" format={timeFormat} minuteStep={15} secondStep={60}/>
            </Card>
          </Col>          
        </Row> 
      </Col>
    </Row>    
  );
};

export default Court01;