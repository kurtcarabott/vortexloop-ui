import React from "react";
import {Card, Col, Row, Calendar} from "antd";

import Court01 from "./Court01'";

const court01Base = () => {
  return (
    <Row>
      <Col lg={6} md={6} sm={12} xs={12}>
        <Court01 />
        <Court01 />
      </Col>      
    </Row>  
  );
};
export default court01Base;
