import React from "react";
import { Button } from "antd";
import IntlMessages from "util/IntlMessages";

const
  PriceItem = ({ styleName, headerStyle, itemStyle, footerStyle, dataStatus, dataNextSchedule, dataMode, dataCourtNumber, dataLink }) => {
    return (
      <div className={`${styleName}`}>
        <div className={`${headerStyle}`}>
          <h2 className="gx-price"><i className="icon icon-halfstar" />{`${dataStatus}`}</h2>
          <p className="gx-letter-spacing-base gx-text-white gx-text-uppercase gx-mb-0">{<IntlMessages id="lightStatus.personal" />}  {`${dataCourtNumber}`}</p>
        </div>

        <div className={`${itemStyle}`}>
          <ul className="gx-package-items">
            <li>
              <i className="icon icon-hotel-booking" />
              <span>{`${dataNextSchedule}`}</span>
            </li>
            <li>
              <i className="icon icon-button" />
              <span>{<IntlMessages id="lightStatus.mode" />} {`${dataMode}`}</span>
            </li>
          </ul>
          <div className="gx-package-footer">
            <Button type="primary" className={`${footerStyle}`} href={`${dataLink}`}>{<IntlMessages
              id="lightStatus.button" />} </Button>
          </div>
        </div>


      </div>
    )
  };

export default PriceItem;

