import React from "react";
import { Col, Row } from "antd";
import LightStatus from "./LightStatus";

const Circle = () => {
  return (
    <div width="100%" className="gx-price-tables gx-pt-circle">
      <Row width="100%">
        <Col xl={6} lg={12} md={12} xs={24}>
          <LightStatus  dataStatus="On" dataNextSchedule="At 21:00" dataMode="Automatic" dataCourtNumber="01" dataLink="light-system/Court01"
            styleName="gx-package"
            headerStyle="gx-package-header gx-bg-yellow gx-text-black"
            itemStyle="gx-package-body"
            footerStyle="gx-btn-block"
          />
        </Col>
        <Col xl={6} lg={12} md={12} xs={24}>
          <LightStatus dataStatus="Off" dataNextSchedule="--" dataMode="Manual" dataCourtNumber="02"  dataLink="light-system/Court02"
            styleName="gx-package"
            headerStyle="gx-package-header gx-bg-grey gx-text-black"
            itemStyle="gx-package-body"
            footerStyle="gx-btn-block"
          />
        </Col>
        <Col xl={6} lg={12} md={12} xs={24}>
          <LightStatus dataStatus="On" dataNextSchedule="In 15 mins" dataMode="Automatic" dataCourtNumber="03" dataLink="light-system/Court03"
            styleName="gx-package"
            headerStyle="gx-package-header gx-bg-yellow gx-text-black"
            itemStyle="gx-package-body"
            footerStyle="gx-btn-block"
          />
        </Col>
        <Col xl={6} lg={12} md={12} xs={24}>
          <LightStatus  dataStatus="On" dataNextSchedule="--" dataMode="Manual" dataCourtNumber="04" dataLink="light-system/Court04"
            styleName="gx-package"
            headerStyle="gx-package-header gx-bg-yellow gx-text-black"
            itemStyle="gx-package-body"
            footerStyle="gx-btn-block"
          />
        </Col>
      </Row>
    </div>
  )
};
export default Circle;

