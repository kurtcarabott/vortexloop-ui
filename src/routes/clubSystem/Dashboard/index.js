import React from "react";
import {Col, Row} from "antd";

import Dashboard from "./Dashboard";

const dashboardBase = () => {
  return (
    <Row>
      <Col lg={12} md={12} sm={24} xs={24}>
        <Dashboard />
      </Col>
    </Row>
  );
};
export default dashboardBase;
