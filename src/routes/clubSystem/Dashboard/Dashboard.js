import React from "react";
import { Breadcrumb, Card, Col, Row } from "antd";
import IntlMessages from "util/IntlMessages";
import LightStatus from "./LightStatus/index";
import ChartCard from "./ChartCard";
import SmartHomeCard from "./SmartHomeCard";
import { Area, AreaChart, ResponsiveContainer, Tooltip } from "recharts";
import { windData } from "../../main/Metrics/data";

const Dashboard = () => {
  return (
    <Row>
      <Col span={24}>
        <Card className="gx-card" title="Dashboard">
          <Breadcrumb>
            <Breadcrumb.Item><span className="gx-link"><a href="/">Home</a></span></Breadcrumb.Item>
            <Breadcrumb.Item><span className="gx-link"><a href="/club-system">Club System</a></span></Breadcrumb.Item>
            <Breadcrumb.Item><span className="gx-link"><a href="/club-system/dashboard">Dashboard</a></span></Breadcrumb.Item>
          </Breadcrumb>
        </Card>
        <Col span={24}>
          <Card className="" width="100%" title={<IntlMessages id="dashboard.lightStatus" />}>
            <LightStatus />
          </Card>
        </Col>
        <Row>
          <Col xl={12} lg={12} md={12} sm={12} xs={24}>
            <ChartCard prize="29 km/h" title="current wind speed" icon="ripple"
              children={<ResponsiveContainer width="100%" height={100}>
                <AreaChart data={windData}
                  margin={{ top: 0, right: 0, left: 0, bottom: 0 }}>
                  <Tooltip />
                  <defs>
                    <linearGradient id="color4" x1="0" y1="0" x2="1" y2="0">
                      <stop offset="5%" stopColor="#4ECDE4" stopOpacity={0.9} />
                      <stop offset="95%" stopColor="#06BB8A" stopOpacity={0.9} />
                    </linearGradient>
                  </defs>
                  <Area dataKey='speed' type='monotone' strokeWidth={0} stackId="2" stroke='#4D95F3'
                    fill="url(#color4)"
                    fillOpacity={1} />
                </AreaChart>
              </ResponsiveContainer>}
              styleName="up" desc="Wind speed" />
          </Col>
          <Col xl={12} lg={12} md={12} sm={12} xs={24}>
            <SmartHomeCard prize="29 km/h" title="current wind speed" icon="ripple"
              styleName="up" desc="Wind speed" />
          </Col>
        </Row>
      </Col>
    </Row>
  );
};

export default Dashboard;