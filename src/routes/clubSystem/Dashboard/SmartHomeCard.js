import React from "react";

import Widget from "components/Widget/index";

const SmartHomeCard = () => {
  return (
    <Widget styleName="gx-card-full">

      <img className="gx-smart-img" alt="Sunlight" src={"https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/2f7cd930-1bb4-4edd-a74a-ac1ad251ee46/d13huhq-ef14c60b-c91d-4436-b970-2d72ed1991c2.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOiIsImlzcyI6InVybjphcHA6Iiwib2JqIjpbW3sicGF0aCI6IlwvZlwvMmY3Y2Q5MzAtMWJiNC00ZWRkLWE3NGEtYWMxYWQyNTFlZTQ2XC9kMTNodWhxLWVmMTRjNjBiLWM5MWQtNDQzNi1iOTcwLTJkNzJlZDE5OTFjMi5qcGcifV1dLCJhdWQiOlsidXJuOnNlcnZpY2U6ZmlsZS5kb3dubG9hZCJdfQ.v0ig1nKF8fRZ_VkPE2oJ3kkAINZcgl42XSZ4nCD8jyc"} />
      <div className="gx-p-3">
        <p className="gx-mb-2">Sunset: 19:32</p>
        <span className="gx-text-primary gx-pointer gx-text-uppercase gx-fs-sm">Daylight Saving Settings (On)</span>
      </div>
    </Widget>
  );
};

export default SmartHomeCard;
