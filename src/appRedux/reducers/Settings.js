import { SWITCH_LANGUAGE, WINDOW_WIDTH } from "constants/ActionTypes";
import {
  LAYOUT_TYPE,
  LAYOUT_TYPE_FULL,
  LAYOUT_TYPE_FRAMED,
  NAV_STYLE,
  NAV_STYLE_FIXED,
  THEME_COLOR,
  DARK_BLUE,
  THEME_TYPE,
  THEME_TYPE_LITE,
  THEME_TYPE_SEMI_DARK, ORANGE, DEEP_ORANGE
} from "../../constants/ThemeSetting";

const initialSettings = {
  navStyle: NAV_STYLE_FIXED,
  layoutType: LAYOUT_TYPE,
  themeType: THEME_TYPE_SEMI_DARK,
  themeColor: DEEP_ORANGE,
  width: window.innerWidth,
  isDirectionRTL: false,
  locale: {
    languageId: 'englishMT',
    locale: 'enMT',
    name: 'English (MT)',
    icon: 'mt'
  }
};

const settings = (state = initialSettings, action) => {
  switch (action.type) {

    case WINDOW_WIDTH:
      return {
        ...state,
        width: action.width,
      };
    case THEME_TYPE:
      return {
        ...state,
        themeType: action.themeType
      };
    case THEME_COLOR:
      console.log("yes", action.themeColor);
      return {
        ...state,
        themeColor: action.themeColor
      };

    case NAV_STYLE:
      return {
        ...state,
        navStyle: action.navStyle
      };
    case LAYOUT_TYPE:
      return {
        ...state,
        layoutType: action.layoutType
      };

    case SWITCH_LANGUAGE:
      return {
        ...state,
        locale: action.payload,

      };
    default:
      return state;
  }
};

export default settings;
