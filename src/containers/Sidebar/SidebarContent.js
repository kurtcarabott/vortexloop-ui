import React from "react";
import { Menu } from "antd";
import { Link } from "react-router-dom";

import CustomScrollbars from "util/CustomScrollbars";
import SidebarLogo from "./SidebarLogo";
import UserProfile from "./UserProfile";
import AppsNavigation from "./AppsNavigation";
import {
  NAV_STYLE_NO_HEADER_EXPANDED_SIDEBAR,
  NAV_STYLE_NO_HEADER_MINI_SIDEBAR,
  THEME_TYPE_LITE
} from "../../constants/ThemeSetting";
import IntlMessages from "../../util/IntlMessages";
import { useSelector } from "react-redux";

const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;


const SidebarContent = () => {

  let { pathname } = useSelector(({ common }) => common);
  let { navStyle, themeType } = useSelector(({ settings }) => settings);

  const getNoHeaderClass = (navStyle) => {
    if (navStyle === NAV_STYLE_NO_HEADER_MINI_SIDEBAR || navStyle === NAV_STYLE_NO_HEADER_EXPANDED_SIDEBAR) {
      return "gx-no-header-notifications";
    }
    return "";
  };
  const getNavStyleSubMenuClass = (navStyle) => {
    if (navStyle === NAV_STYLE_NO_HEADER_MINI_SIDEBAR) {
      return "gx-no-header-submenu-popup";
    }
    return "";
  };
  const selectedKeys = pathname.substr(1);
  const defaultOpenKeys = selectedKeys.split('/')[1];
  return (
    <>
      <SidebarLogo />
      <div className="gx-sidebar-content">
        <div className={`gx-sidebar-notifications ${getNoHeaderClass(navStyle)}`}>
          <UserProfile />
          {/* <AppsNavigation/> */}
        </div>
        <CustomScrollbars className="gx-layout-sider-scrollbar">
          <Menu
            defaultOpenKeys={[defaultOpenKeys]}
            selectedKeys={[selectedKeys]}
            theme={themeType === THEME_TYPE_LITE ? 'lite' : 'dark'}
            mode="inline">

            <MenuItemGroup key="main" className="gx-menu-group"
              title={
                <Link to="/club-system">
                  <span>
                    <span><IntlMessages id="sidebar.clubsystem" /></span>
                  </span>
                </Link>
              }>

              <Menu.Item key="club-system/dashboard">
                <Link to="/club-system/Dashboard">
                  <i className="icon icon-dasbhoard" />
                  <span><IntlMessages id="sidebar.clubsystem.dashboard" /></span>
                </Link>
              </Menu.Item>

              <SubMenu key="lightsystem" popupClassName={getNavStyleSubMenuClass(navStyle)}
                title={
                  <Link to="/club-system/light-system">
                    <span>
                      <i className="icon icon-eye" />
                      <span><IntlMessages id="sidebar.clubsystem.lightsystem" /></span>
                    </span>
                  </Link>
                }>

                <Menu.Item key="club-system/light-system/court01">
                  <Link to="/club-system/light-system/Court01">
                    <i className="icon icon-radiobutton" />
                    <span><IntlMessages id="sidebar.clubsystem.lightsystem.court01" /></span>
                  </Link>
                </Menu.Item>

                <Menu.Item key="club-system/light-system/court02">
                  <Link to="/club-system/light-system/Court02">
                    <i className="icon icon-radiobutton" />
                    <span><IntlMessages id="sidebar.clubsystem.lightsystem.court02" /></span>
                  </Link>
                </Menu.Item>

                <Menu.Item key="club-system/light-system/court03">
                  <Link to="/club-system/light-system/Court03">
                    <i className="icon icon-radiobutton" />
                    <span><IntlMessages id="sidebar.clubsystem.lightsystem.court03" /></span>
                  </Link>
                </Menu.Item>

                <Menu.Item key="club-system/light-system/court04">
                  <Link to="/club-system/light-system/Court04">
                    <i className="icon icon-radiobutton" />
                    <span><IntlMessages id="sidebar.clubsystem.lightsystem.court04" /></span>
                  </Link>
                </Menu.Item>

              </SubMenu>

            </MenuItemGroup>

          </Menu>
        </CustomScrollbars>
      </div>
    </>
  );
};

SidebarContent.propTypes = {};
export default SidebarContent;

