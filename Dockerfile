FROM node:latest AS installer
ADD package*.json yarn.lock /app/
WORKDIR /app
RUN yarn install

FROM node:latest AS builder
COPY --from=installer /app /app
ADD . /app
WORKDIR /app
RUN yarn build

FROM node:latest AS publisher
RUN mkdir -p /app/build
COPY --from=builder /app/build /app/build
WORKDIR /app
RUN yarn global add serve

EXPOSE 5000

#RUN ls
CMD serve -s build